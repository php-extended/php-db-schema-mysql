<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\MysqlFunctionFloor;
use PHPUnit\Framework\TestCase;

/**
 * MysqlFunctionFloorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\MysqlFunctionFloor
 *
 * @internal
 *
 * @small
 */
class MysqlFunctionFloorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MysqlFunctionFloor
	 */
	protected MysqlFunctionFloor $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}

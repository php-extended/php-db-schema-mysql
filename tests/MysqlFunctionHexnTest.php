<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\MysqlFunctionHexn;
use PHPUnit\Framework\TestCase;

/**
 * MysqlFunctionHexnTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\MysqlFunctionHexn
 *
 * @internal
 *
 * @small
 */
class MysqlFunctionHexnTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MysqlFunctionHexn
	 */
	protected MysqlFunctionHexn $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}

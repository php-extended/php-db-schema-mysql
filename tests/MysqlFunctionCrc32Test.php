<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\MysqlFunctionCrc32;
use PHPUnit\Framework\TestCase;

/**
 * MysqlFunctionCrc32Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\MysqlFunctionCrc32
 *
 * @internal
 *
 * @small
 */
class MysqlFunctionCrc32Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MysqlFunctionCrc32
	 */
	protected MysqlFunctionCrc32 $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\CharacterSetReferenceHolder;
use PHPUnit\Framework\TestCase;

/**
 * CharacterSetReferenceHolderTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\CharacterSetReferenceHolder
 * @internal
 * @small
 */
class CharacterSetReferenceHolderTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CharacterSetReferenceHolder
	 */
	protected CharacterSetReferenceHolder $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CharacterSetReferenceHolder();
	}
	
}

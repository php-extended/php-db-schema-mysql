<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\MysqlFunctionLn;
use PHPUnit\Framework\TestCase;

/**
 * MysqlFunctionLnTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\MysqlFunctionLn
 *
 * @internal
 *
 * @small
 */
class MysqlFunctionLnTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MysqlFunctionLn
	 */
	protected MysqlFunctionLn $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}

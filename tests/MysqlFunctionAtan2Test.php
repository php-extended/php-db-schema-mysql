<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\MysqlFunctionAtan2;
use PHPUnit\Framework\TestCase;

/**
 * MysqlFunctionAtan2Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\MysqlFunctionAtan2
 *
 * @internal
 *
 * @small
 */
class MysqlFunctionAtan2Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MysqlFunctionAtan2
	 */
	protected MysqlFunctionAtan2 $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}

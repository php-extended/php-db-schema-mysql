<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\MysqlFunctionFormat;
use PHPUnit\Framework\TestCase;

/**
 * MysqlFunctionFormatTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\MysqlFunctionFormat
 *
 * @internal
 *
 * @small
 */
class MysqlFunctionFormatTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MysqlFunctionFormat
	 */
	protected MysqlFunctionFormat $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}

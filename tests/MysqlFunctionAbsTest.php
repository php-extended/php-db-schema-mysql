<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\MysqlFunctionAbs;
use PHPUnit\Framework\TestCase;

/**
 * MysqlFunctionAbsTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\MysqlFunctionAbs
 *
 * @internal
 *
 * @small
 */
class MysqlFunctionAbsTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MysqlFunctionAbs
	 */
	protected MysqlFunctionAbs $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}

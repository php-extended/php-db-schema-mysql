<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\MysqlFunctionLog2;
use PHPUnit\Framework\TestCase;

/**
 * MysqlFunctionLog2Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\MysqlFunctionLog2
 *
 * @internal
 *
 * @small
 */
class MysqlFunctionLog2Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MysqlFunctionLog2
	 */
	protected MysqlFunctionLog2 $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\MysqlTypeString;
use PHPUnit\Framework\TestCase;

/**
 * MysqlTypeStringTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\MysqlTypeString
 *
 * @internal
 *
 * @small
 */
class MysqlTypeStringTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MysqlTypeString
	 */
	protected MysqlTypeString $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\MysqlFunctionLog10;
use PHPUnit\Framework\TestCase;

/**
 * MysqlFunctionLog10Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\MysqlFunctionLog10
 *
 * @internal
 *
 * @small
 */
class MysqlFunctionLog10Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MysqlFunctionLog10
	 */
	protected MysqlFunctionLog10 $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}

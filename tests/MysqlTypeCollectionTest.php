<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\MysqlTypeCollection;
use PHPUnit\Framework\TestCase;

/**
 * MysqlTypeCollectionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\MysqlTypeCollection
 *
 * @internal
 *
 * @small
 */
class MysqlTypeCollectionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MysqlTypeCollection
	 */
	protected MysqlTypeCollection $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\MysqlDialect;
use PHPUnit\Framework\TestCase;

/**
 * MysqlDialectTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\MysqlDialect
 *
 * @internal
 *
 * @small
 */
class MysqlDialectTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MysqlDialect
	 */
	protected MysqlDialect $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MysqlDialect();
	}
	
}

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

use PhpExtended\Charset\CharacterSetReference;

final class CharacterSetReferenceHolder
{

	/**
	 * The instance of character set reference.
	 * 
	 * @var ?CharacterSetReference
	 */
	private static $_instance = null;

	/**
	 * Gets the character set reference from this holder.
	 * 
	 * @return CharacterSetReference
	 */
	public static function get() : CharacterSetReference
	{
		if(null === static::$_instance)
		{
			static::$_instance = new CharacterSetReference();
		}

		return static::$_instance;
	}

}

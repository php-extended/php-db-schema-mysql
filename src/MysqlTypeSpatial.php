<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlTypeSpatial class file.
 *
 * This class represents all the valid spatial types for mysql.
 *
 * @author Anastaszor
 */
enum MysqlTypeSpatial : string implements TypeSpatialInterface
{
	
	
	/**
	 * Gets a spatial type of the given complexity.
	 * 
	 * @param integer $complexity
	 * @return MysqlTypeSpatial
	 */
	public static function findByComplexity(int $complexity) : MysqlTypeSpatial
	{
		foreach(static::cases() as $value)
		{
			if($value->getComplexity() === $complexity)
			{
				return $value;
			}
		}
		
		return static::GEOMETRYCOLLECTION;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeInterface::getName()
	 */
	public function getName() : string
	{
		return $this->value;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeSpatialInterface::getComplexity()
	 */
	public function getComplexity() : int
	{
		return match($this)
		{
			self::POINT => 0,
			self::MULTIPOINT => 1,
			self::LINESTRING => 2,
			self::MULTILINESTRING => 3,
			self::POLYGON => 4,
			self::MULTIPOLYGON => 5,
			self::GEOMETRY => 6,
			self::GEOMETRYCOLLECTION => 7,
		};
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeSpatialInterface::castTo()
	 */
	public function castTo(TypeSpatialInterface $type, StatementValueSpatialInterface $statement) : StatementValueSpatialInterface
	{
		// spatial values cant be cast with mysql
		return $statement;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeSpatialInterface::mergeWith()
	 */
	public function mergeWith(TypeSpatialInterface $type) : TypeSpatialInterface
	{
		if($this->getComplexity() > $type->getComplexity())
		{
			return $this;
		}
		if($type->getComplexity() > $this->getComplexity())
		{
			return $type;
		}
		
		return static::findByComplexity(\max(1 + $this->getComplexity(), 1 + $type->getComplexity()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(TypeVisitorInterface $visitor)
	{
		return $visitor->visitTypeSpatial($this);
	}

	case POINT = 'point';
	case MULTIPOINT = 'multipoint';
	case LINESTRING = 'linestring';
	case MULTILINESTRING = 'multilinestring';
	case POLYGON = 'polygon';
	case MULTIPOLYGON = 'multipolygon';
	case GEOMETRY = 'geometry';
	case GEOMETRYCOLLECTION = 'geometrycollection';
	
}

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionConv class file.
 * 
 * Converts numbers between different number bases. Returns a string
 * representation of the number N, converted from base from_base to base
 * to_base. Returns NULL if any argument is NULL. The argument N is
 * interpreted as an integer, but may be specified as an integer or a string.
 * The minimum base is 2 and the maximum base is 36. If from_base is a
 * negative number, N is regarded as a signed number. Otherwise, N is treated
 * as unsigned. CONV() works with 64-bit precision. 
 * 
 * @author Anastaszor
 */
class MysqlFunctionConv extends StatementFunctionCallString
{
	
	/**
	 * Builds a new MysqlFunctionConv with the given arguments.
	 * 
	 * @param StatementValueStringInterface $arg1
	 * @param StatementValueNumberInterface $fromBase
	 * @param StatementValueNumberInterface $toBase
	 */
	public function __construct(StatementValueStringInterface $arg1, StatementValueNumberInterface $fromBase, StatementValueNumberInterface $toBase)
	{
		parent::__construct('CONV', $arg1->getType(), [$arg1, $fromBase, $toBase]);
	}
	
}

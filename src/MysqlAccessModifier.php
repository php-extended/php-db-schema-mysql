<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlAccessModifier class file.
 * 
 * This class represents an assignment modifier for mysql.
 * 
 * @author Anastaszor
 */
enum MysqlAccessModifier : string
{
	
	case NONE = 'none';
	case NEW = 'new';
	case OLD = 'old';
	
}

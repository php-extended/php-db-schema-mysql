<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionLn class file.
 * 
 * This class represents the natural logarithm of X; that is, the base-e
 * logarithm of X. If X is less than or equal to 0, then NULL is returned.
 * 
 * @author Anastaszor
 */
class MysqlFunctionLn extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionLn with the given arguments.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 */
	public function __construct(StatementValueNumberInterface $arg1)
	{
		parent::__construct('LN', MysqlTypeNumber::DOUBLE, [$arg1]);
	}
	
}

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionHexn class file.
 * 
 * This class represents a hexadecimal representation of a decimal number.
 * 
 * @author Anastaszor
 */
class MysqlFunctionHexn extends StatementFunctionCallString
{
	
	/**
	 * Builds a new MysqlFunctionHexn with the given argument.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 */
	public function __construct(StatementValueNumberInterface $arg1)
	{
		parent::__construct('HEX', MysqlTypeString::VARCHAR, [$arg1]);
	}
	
}

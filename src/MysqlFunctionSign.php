<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionSign class file.
 * 
 * This class corresponds to the sign of the argument as -1, 0, or 1, depending
 * on whether X is negative, zero, or positive.
 * 
 * @author Anastaszor
 */
class MysqlFunctionSign extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionSign with the given arguments.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 */
	public function __construct(StatementValueNumberInterface $arg1)
	{
		parent::__construct('SIGN', MysqlTypeNumber::TINYINT, [$arg1]);
	}
	
}

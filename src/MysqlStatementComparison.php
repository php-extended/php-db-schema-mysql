<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlStatementComparison class file.
 * 
 * This class is the reification for the mysql schema of the statement comparison.
 * 
 * @author Anastaszor
 */
class MysqlStatementComparison extends AbstractStatementComparison
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueNumberInterface::getType()
	 */
	public function getType() : TypeNumberInterface
	{
		return MysqlTypeNumber::BOOLEAN;
	}
	
}

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionAtan2 class file.
 * 
 * This class represents the arc tangent of the two variables X and Y. It is
 * similar to calculating the arc tangent of Y / X, except that the signs of
 * both arguments are used to determine the quadrant of the result.
 * 
 * @author Anastaszor
 */
class MysqlFunctionAtan2 extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionAtan2 with the given arguments.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 * @param StatementValueNumberInterface $arg2
	 */
	public function __construct(StatementValueNumberInterface $arg1, StatementValueNumberInterface $arg2)
	{
		parent::__construct('ATAN2', MysqlTypeNumber::DOUBLE, [$arg1, $arg2]);
	}
	
}

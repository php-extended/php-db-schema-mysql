<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionRadians class file.
 * 
 * This class represents the argument X, converted from degrees to radians.
 * 
 * @author Anastaszor
 */
class MysqlFunctionRadians extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionRadians with the given argument.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 */
	public function __construct(StatementValueNumberInterface $arg1)
	{
		parent::__construct('RADIANS', MysqlTypeNumber::DOUBLE, [$arg1]);
	}
	
}

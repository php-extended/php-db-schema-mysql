<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionLog class file.
 * 
 * This class represents the logarithm of X to the base B. If X is less than
 * or equal to 0, or if B is less than or equal to 1, then NULL is returned.
 * LOG(B,X) is equivalent to LN(X) / LN(B).
 * 
 * @author Anastaszor
 */
class MysqlFunctionLog extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionLog with the given parameters.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 * @param StatementValueNumberInterface $arg2
	 */
	public function __construct(StatementValueNumberInterface $arg1, StatementValueNumberInterface $arg2)
	{
		parent::__construct('LOG', MysqlTypeNumber::DOUBLE, [$arg1, $arg2]);
	}
	
}

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionLog2 class file.
 * 
 * This class represents the base-2 logarithm of X. LOG2() is useful for
 * finding out how many bits a number requires for storage. This function is
 * equivalent to the expression LN(X) / LN(2).
 * 
 * @author Anastaszor
 */
class MysqlFunctionLog2 extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionLog2 with the given arguments.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 */
	public function __construct(StatementValueNumberInterface $arg1)
	{
		parent::__construct('LOG2', MysqlTypeNumber::DOUBLE, [$arg1]);
	}
	
}

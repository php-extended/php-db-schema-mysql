<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionMod class file.
 * 
 * This class represents the modulo operation. Returns the remainder of N
 * divided by M. This function is safe to use with BIGINT values. MOD() also
 * works on values that have a fractional part and returns the exact remainder
 * after division. MOD(N,0) returns NULL.
 * 
 * @author Anastaszor
 */
class MysqlFunctionMod extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionMod with the given arguments.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 * @param StatementValueNumberInterface $arg2
	 */
	public function __construct(StatementValueNumberInterface $arg1, StatementValueNumberInterface $arg2)
	{
		parent::__construct('MOD', $arg1->getType()->mergeWith($arg2->getType()), [$arg1, $arg2]);
	}
	
}

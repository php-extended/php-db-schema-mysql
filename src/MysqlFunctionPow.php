<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionPow class file.
 * 
 * This class represents the value of X raised to the power of Y.
 * 
 * @author Anastaszor
 */
class MysqlFunctionPow extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionPow with the given arguments.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 * @param StatementValueNumberInterface $arg2
	 */
	public function __construct(StatementValueNumberInterface $arg1, StatementValueNumberInterface $arg2)
	{
		parent::__construct('POW', $arg1->getType()->mergeWith($arg2->getType()), [$arg1, $arg2]);
	}
	
}

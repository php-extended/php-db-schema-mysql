<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

use ReflectionException;

/**
 * MysqlDialect class file.
 *
 * This class represents a dialect for mysql.
 *
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyMethods")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class MysqlDialect implements StatementVisitorInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\DialectInterface::createDatabase()
	 */
	public function createDatabase(DatabaseInterface $database) : string
	{
		$sql = 'CREATE DATABASE '.$this->quoteDatabaseName($database->getName());
		$sql .= ' DEFAULT COLLATE = '.$database->getDefaultCollation()->getName();
		$sql .= ";\n";
		
		return $sql;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\DialectInterface::dropDatabase()
	 */
	public function dropDatabase(DatabaseInterface $database) : string
	{
		$sql = 'DROP DATABASE IF EXISTS '.$this->quoteDatabaseName($database->getName());
		$sql .= ";\n";
		
		return $sql;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\DialectInterface::createSchema()
	 */
	public function createSchema(SchemaInterface $schema) : string
	{
		return '';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\DialectInterface::dropSchema()
	 */
	public function dropSchema(SchemaInterface $schema) : string
	{
		return '';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\DialectInterface::createTable()
	 * @throws ReflectionException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public function createTable(TableInterface $table) : string
	{
		$sql = 'CREATE TABLE '.$this->quoteTableName($table->getName())."\n";
		$sql .= "(\n";
		$stsql = [];
		
		foreach($table->getColumns() as $tableColumn)
		{
			$subsql = $this->quoteColumnName($tableColumn->getName()).' ';
			
			switch(true)
			{
				case $tableColumn instanceof ColumnCollectionInterface:
					$subsql .= $tableColumn->getType()->getName().' ('.\implode(',', $tableColumn->getValues()).') ';
					break;
				
				case $tableColumn instanceof ColumnDatetimeInterface:
					$subsql .= $tableColumn->getType()->getName().' ';
					if($tableColumn->hasAutoupdate())
					{
						$subsql .= ' ON UPDATE CURRENT_TIMESTAMP ';
					}
					break;
				
				case $tableColumn instanceof ColumnJsonInterface:
					$subsql .= $tableColumn->getType()->getName().' ';
					break;
				
				case $tableColumn instanceof ColumnNumberInterface:
					$subsql .= $tableColumn->getType()->getName();
					if(MysqlTypeNumber::BOOLEAN !== $tableColumn->getType() && MysqlTypeNumber::SERIAL !== $tableColumn->getType() && !empty($tableColumn->getLength()))
					{
						$subsql .= '('.((string) $tableColumn->getLength());
						if((int) $tableColumn->getPrecision() > 0)
						{
							$subsql .= ','.((string) $tableColumn->getPrecision());
						}
						$subsql .= ')';
					}
					// booleans cannot be unsigned nor autoincremented
					if($tableColumn->isUnsigned() && false === \mb_stripos($tableColumn->getType()->getName(), 'bool'))
					{
						$subsql .= ' UNSIGNED';
					}
					if($tableColumn->hasAutoIncrement() && false === \mb_stripos($tableColumn->getType()->getName(), 'bool'))
					{
						$subsql .= ' AUTO_INCREMENT';
					}
					$subsql .= ' ';
					break;
				
				case $tableColumn instanceof ColumnSpatialInterface:
					$subsql .= $tableColumn->getType()->getName().' ';
					break;
				
				case $tableColumn instanceof ColumnStringInterface:
					$subsql .= $tableColumn->getType()->getName();
					if(!empty($tableColumn->getLength()))
					{
						$subsql .= '('.((string) $tableColumn->getLength()).')';
					}
					$subsql .= ' COLLATE '.$tableColumn->getCollation()->getName();
					$subsql .= ' ';
					break;
				
				case $tableColumn instanceof ColumnTextInterface:
					$subsql .= $tableColumn->getType()->getName();
					$subsql .= ' COLLATE '.$tableColumn->getCollation()->getName();
					$subsql .= ' ';
					break;
			}
			
			$subsql .= $tableColumn->isNullAllowed() ? 'DEFAULT NULL ' : 'NOT NULL ';
			
			if(!empty($tableColumn->getComment()))
			{
				$subsql .= 'COMMENT '.$this->quoteString($tableColumn->getComment());
			}
			
			$stsql[] = $subsql;
		}
		
		$stsql[] = 'PRIMARY KEY ('.\implode(', ', \array_map(
			[$this, 'quoteColumnName'],
			$table->getPrimaryKey()->getColumnNames(),
		)).')';
		
		foreach($table->getIndexes() as $index)
		{
			$stsql[] = ($index->isUnique() ? 'UNIQUE KEY ' : 'KEY ')
				.$this->quoteTableName($index->getName()).' ('.\implode(', ', \array_map(
					[$this, 'quoteColumnName'],
					$index->getColumnNames(),
				)).')';
		}
		$sql .= "\t".\implode(",\n\t", $stsql)."\n";
		$sql .= ")\n";
		
		$sql .= 'ENGINE='.$table->getEngine()->getName()."\n";
		
		$mysqlCharset = MysqlCharacterSet::findByCharset($table->getDefaultCollation()->getCharacterSet());
		$sql .= 'DEFAULT CHARSET='.$mysqlCharset->value
			.' COLLATE='.$table->getDefaultCollation()->getName()."\n";
		
		$comment = $table->getComment();
		if(null !== $comment)
		{
			$sql .= 'COMMENT '.$this->quoteString($comment);
		}
		$sql .= ";\n";
		
		return $sql;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\DialectInterface::dropTable()
	 */
	public function dropTable(TableInterface $table) : string
	{
		return 'DROP TABLE IF EXISTS '.$this->quoteTableName($table->getName()).";\n";
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\DialectInterface::truncateTable()
	 */
	public function truncateTable(TableInterface $table) : string
	{
		return 'TRUNCATE TABLE '.$this->quoteTableName($table->getName()).";\n";
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\DialectInterface::alterTableAddForeignKey()
	 */
	public function alterTableAddForeignKey(TableInterface $sourceTable, ForeignKeyInterface $foreignKey) : string
	{
		$sql = 'ALTER TABLE '.$this->quoteTableName($sourceTable->getName());
		// mysql limit s 64 chars for constraint name
		$sql .= ' ADD CONSTRAINT '.$this->quoteSingleColumnName((string) \mb_substr($foreignKey->getName(), 0, 64));
		$sql .= ' FOREIGN KEY (';
		$sql .= \implode(', ', \array_map(function(ForeignKeyRelationInterface $foreignKeyRelation)
		{
			return $this->quoteColumnName($foreignKeyRelation->getSourceFieldName());
		}, \iterator_to_array($foreignKey->getForeignKeyRelations())));
		$sql .= ')';
		$sql .= ' REFERENCES '.$this->quoteTableName($foreignKey->getTargetTableName());
		$sql .= ' (';
		$sql .= \implode(', ', \array_map(function(ForeignKeyRelationInterface $foreignKeyRelation)
		{
			return $this->quoteColumnName($foreignKeyRelation->getTargetFieldName());
		}, \iterator_to_array($foreignKey->getForeignKeyRelations())));
		$sql .= ')';
		$sql .= ' ON UPDATE '.$foreignKey->getOnUpdateAction()->getName();
		$sql .= ' ON DELETE '.$foreignKey->getOnDeleteAction()->getName();
		$sql .= ";\n";
		
		return $sql;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\DialectInterface::alterTableDropForeignKey()
	 */
	public function alterTableDropForeignKey(TableInterface $sourceTable, ForeignKeyInterface $foreignKey) : string
	{
		$sql = 'ALTER TABLE '.$this->quoteTableName($sourceTable->getName());
		$sql .= ' DROP CONSTRAINT IF EXISTS ';
		$sql .= $this->quoteSingleColumnName((string) \mb_substr($foreignKey->getName(), 0, 64));
		$sql .= ";\n";
		
		return $sql;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\DialectInterface::createTrigger()
	 */
	public function createTrigger(TableInterface $table, TriggerInterface $trigger) : string
	{
		$sql = 'DELIMITER $$'."\n";
		$sql .= 'CREATE TRIGGER '.$this->quoteSingleColumnName($trigger->getName());
		$sql .= ' '.$trigger->getHandler()->getHandlerName();
		$sql .= ' '.$trigger->getEvent()->getEventName();
		$sql .= ' ON '.$this->quoteTableName($table->getName())."\n";
		$sql .= "FOR EACH ROW BEGIN\n";
		
		foreach($trigger->getStatements() as $statement)
		{
			/** @phpstan-ignore-next-line */ /** @psalm-suppress PossiblyInvalidCast */
			$sql .= "\t".((string) $statement->beVisitedBy($this))."\n";
		}
		$sql .= 'END $$'."\n";
		$sql .= "DELIMITER ;\n";
		
		return $sql;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\DialectInterface::dropTrigger()
	 */
	public function dropTrigger(TableInterface $table, TriggerInterface $trigger) : string
	{
		return 'DROP TRIGGER IF EXISTS '.$this->quoteSingleColumnName($trigger->getName()).";\n";
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitStatementAssignation()
	 */
	public function visitStatementAssignation(StatementAssignationInterface $statement) : string
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress PossiblyInvalidCast */
		return 'SET '.((string) $statement->getVariable()->beVisitedBy($this)).' '.$statement->getOperator().' '.((string) $statement->getStatement()->beVisitedBy($this)).';';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitStatementComparison()
	 */
	public function visitStatementComparison(StatementComparisonInterface $statement) : string
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress PossiblyInvalidCast */
		return ((string) $statement->getLeftOperand()->beVisitedBy($this)).' '.$statement->getOperator().' '.((string) $statement->getRightOperand()->beVisitedBy($this));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitStatementDeclaration()
	 */
	public function visitStatementDeclaration(StatementDeclarationInterface $statement) : string
	{
		return 'DECLARE '.$statement->getVariableName()->getVariableName()
			.' '.$statement->getVariableType()->getName().';';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFixedValueDatetime()
	 */
	public function visitFixedValueDatetime(StatementFixedValueDatetimeInterface $statement) : string
	{
		return $this->quoteString($statement->getFixedValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFixedValueJson()
	 */
	public function visitFixedValueJson(StatementFixedValueJsonInterface $statement) : string
	{
		return $this->quoteString($statement->getFixedValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFixedValueNumber()
	 */
	public function visitFixedValueNumber(StatementFixedValueNumberInterface $statement) : string
	{
		return $statement->getFixedValue();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFixedValueSpatial()
	 */
	public function visitFixedValueSpatial(StatementFixedValueSpatialInterface $statement) : string
	{
		return $this->quoteString($statement->getFixedValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFixedValueString()
	 */
	public function visitFixedValueString(StatementFixedValueStringInterface $statement) : string
	{
		return $this->quoteString($statement->getFixedValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFixedValueText()
	 */
	public function visitFixedValueText(StatementFixedValueTextInterface $statement) : string
	{
		return $this->quoteString($statement->getFixedValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitVariableDatetime()
	 */
	public function visitVariableDatetime(StatementVariableDatetimeInterface $statement) : string
	{
		return $statement->getVariableName();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitVariableJson()
	 */
	public function visitVariableJson(StatementVariableJsonInterface $statement) : string
	{
		return $statement->getVariableName();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitVariableNumber()
	 */
	public function visitVariableNumber(StatementVariableNumberInterface $statement) : string
	{
		return $statement->getVariableName();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitVariableSpatial()
	 */
	public function visitVariableSpatial(StatementVariableSpatialInterface $statement) : string
	{
		return $statement->getVariableName();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitVariableString()
	 */
	public function visitVariableString(StatementVariableStringInterface $statement) : string
	{
		return $statement->getVariableName();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitVariableText()
	 */
	public function visitVariableText(StatementVariableTextInterface $statement) : string
	{
		return $statement->getVariableName();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFunctionCallDatetime()
	 */
	public function visitFunctionCallDatetime(StatementFunctionCallDatetimeInterface $statement) : string
	{
		return $statement->getFunctionName().'('
			.\implode(', ', \array_map(
				function(StatementValueInterface $value) : string
				{
					/** @phpstan-ignore-next-line */ /** @psalm-suppress PossiblyInvalidCast */
					return (string) $value->beVisitedBy($this);
				},
				\iterator_to_array($statement->getParameters()),
			))
		.')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFunctionCallJson()
	 */
	public function visitFunctionCallJson(StatementFunctionCallJsonInterface $statement) : string
	{
		return $statement->getFunctionName().'('
			.\implode(', ', \array_map(
				function(StatementValueInterface $value) : string
				{
					/** @phpstan-ignore-next-line */ /** @psalm-suppress PossiblyInvalidCast */
					return (string) $value->beVisitedBy($this);
				},
				\iterator_to_array($statement->getParameters()),
			))
		.')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFunctionCallNumber()
	 */
	public function visitFunctionCallNumber(StatementFunctionCallNumberInterface $statement) : string
	{
		return $statement->getFunctionName().'('
			.\implode(', ', \array_map(
				function(StatementValueInterface $value) : string
				{
					/** @phpstan-ignore-next-line */ /** @psalm-suppress PossiblyInvalidCast */
					return (string) $value->beVisitedBy($this);
				},
				\iterator_to_array($statement->getParameters()),
			))
		.')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFunctionCallSpatial()
	 */
	public function visitFunctionCallSpatial(StatementFunctionCallSpatialInterface $statement) : string
	{
		return $statement->getFunctionName().'('
			.\implode(', ', \array_map(
				function(StatementValueInterface $value) : string
				{
					/** @phpstan-ignore-next-line */ /** @psalm-suppress PossiblyInvalidCast */
					return (string) $value->beVisitedBy($this);
				},
				\iterator_to_array($statement->getParameters()),
			))
		.')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFunctionCallString()
	 */
	public function visitFunctionCallString(StatementFunctionCallStringInterface $statement) : string
	{
		return $statement->getFunctionName().'('
			.\implode(', ', \array_map(
				function(StatementValueInterface $value) : string
				{
					/** @phpstan-ignore-next-line */ /** @psalm-suppress PossiblyInvalidCast */
					return (string) $value->beVisitedBy($this);
				},
				\iterator_to_array($statement->getParameters()),
			))
		.')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFunctionCallText()
	 */
	public function visitFunctionCallText(StatementFunctionCallTextInterface $statement) : string
	{
		return $statement->getFunctionName().'('
			.\implode(', ', \array_map(
				function(StatementValueInterface $value) : string
				{
					/** @phpstan-ignore-next-line */ /** @psalm-suppress PossiblyInvalidCast */
					return (string) $value->beVisitedBy($this);
				},
				\iterator_to_array($statement->getParameters()),
			))
		.')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitColumnDatetime()
	 */
	public function visitColumnDatetime(StatementColumnDatetimeInterface $statement) : string
	{
		$str = $statement->getAccessModifier()->getModifier().'.';
		$str .= $this->quoteColumnName($statement->getColumn()->getName());
		
		return $str;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitColumnJson()
	 */
	public function visitColumnJson(StatementColumnJsonInterface $statement) : string
	{
		$str = $statement->getAccessModifier()->getModifier().'.';
		$str .= $this->quoteColumnName($statement->getColumn()->getName());
		
		return $str;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitColumnNumber()
	 */
	public function visitColumnNumber(StatementColumnNumberInterface $statement) : string
	{
		$str = $statement->getAccessModifier()->getModifier().'.';
		$str .= $this->quoteColumnName($statement->getColumn()->getName());
		
		return $str;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitColumnSpatial()
	 */
	public function visitColumnSpatial(StatementColumnSpatialInterface $statement) : string
	{
		$str = $statement->getAccessModifier()->getModifier().'.';
		$str .= $this->quoteColumnName($statement->getColumn()->getName());
		
		return $str;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitColumnString()
	 */
	public function visitColumnString(StatementColumnStringInterface $statement) : string
	{
		$str = $statement->getAccessModifier()->getModifier().'.';
		$str .= $this->quoteColumnName($statement->getColumn()->getName());
		
		return $str;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitColumnText()
	 */
	public function visitColumnText(StatementColumnTextInterface $statement) : string
	{
		$str = $statement->getAccessModifier()->getModifier().'.';
		$str .= $this->quoteColumnName($statement->getColumn()->getName());
		
		return $str;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::quoteColumnName()
	 */
	public function quoteColumnName(string $columnName) : string
	{
		return \implode('.', \array_map([$this, 'quoteSingleColumnName'], \explode('.', $columnName)));
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::quoteTableName()
	 */
	public function quoteTableName(string $tableName) : string
	{
		return \implode('.', \array_map([$this, 'quoteSingleTableName'], \explode('.', $tableName)));
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\AbstractDialect::quoteDatabaseName()
	 */
	public function quoteDatabaseName(string $databaseName) : string
	{
		return \implode('.', \array_map([$this, 'quoteSingleDatabaseName'], \explode('.', $databaseName)));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\AbstractDialect::quoteSingleColumnName()
	 */
	protected function quoteSingleColumnName(string $columnName) : string
	{
		return '`'.\strtr($columnName, '`', '``').'`';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\AbstractDialect::quoteSingleTableName()
	 */
	protected function quoteSingleTableName(string $tableName) : string
	{
		return '`'.\strtr($tableName, '`', '``').'`';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\AbstractDialect::quoteSingleDatabaseName()
	 */
	protected function quoteSingleDatabaseName(string $databaseName) : string
	{
		return '`'.\strtr($databaseName, '`', '``').'`';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\AbstractDialect::quoteString()
	 */
	protected function quoteString(string $string) : string
	{
		return "'".\strtr($string, "'", "''")."'";
	}
	
}

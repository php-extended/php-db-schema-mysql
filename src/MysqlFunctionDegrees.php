<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionDegrees class file.
 * 
 * This class represents the argument X, converted from radians to degrees.
 * 
 * @author Anastaszor
 */
class MysqlFunctionDegrees extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionDegrees with the given argument.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 */
	public function __construct(StatementValueNumberInterface $arg1)
	{
		parent::__construct('DEGREES', MysqlTypeNumber::DOUBLE, [$arg1]);
	}
	
}

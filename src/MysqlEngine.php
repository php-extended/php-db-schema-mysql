<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlEngine class file.
 *
 * This class represents all valid storage engines for mysql.
 *
 * @author Anastaszor
 */
enum MysqlEngine : string
{
	
	case INNODB = 'innodb';
	case MRG_MYISAM = 'mrg_myisam';
	case MEMORY = 'memory';
	case BLACKHOLE = 'blackhole';
	case MYISAM = 'myisam';
	case CSV = 'csv';
	case ARCHIVE = 'archive';
	case PERFORMANCE_SCHEMA = 'performance_schema';
	case FEDERATED = 'federated';
	
}

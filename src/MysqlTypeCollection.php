<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlTypeCollection class file.
 *
 * This class represents all the valid collection types for mysql.
 *
 * @author Anastaszor
 */
enum MysqlTypeCollection : string implements TypeCollectionInterface
{

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeInterface::getName()
	 */
	public function getName() : string
	{
		return $this->value;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeCollectionInterface::allowsMultiple()
	 */
	public function allowsMultiple() : bool
	{
		return 'set' === $this->value;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeCollectionInterface::getMaxElements()
	 */
	public function getMaxElements() : int
	{
		return match($this)
		{
			self::ENUM => 65535,
			self::SET => 64,
		};
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeCollectionInterface::mergeWith()
	 */
	public function mergeWith(TypeCollectionInterface $type) : TypeCollectionInterface
	{
		if($this->allowsMultiple())
		{
			return $this;
		}

		if($type->allowsMultiple())
		{
			return $type;
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(TypeVisitorInterface $visitor)
	{
		return $visitor->visitTypeCollection($this);
	}

	case ENUM = 'enum';
	case SET = 'set';
	
}

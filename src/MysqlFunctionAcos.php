<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionAcos class file.
 * 
 * This class represents the the arc cosine of X, that is, the value whose
 * cosine is X. Returns NULL if X is not in the range -1 to 1.
 * 
 * @author Anastaszor
 */
class MysqlFunctionAcos extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionAcos with the given argument.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 */
	public function __construct(StatementValueNumberInterface $arg1)
	{
		parent::__construct('ACOS', MysqlTypeNumber::DOUBLE, [$arg1]);
	}
	
}

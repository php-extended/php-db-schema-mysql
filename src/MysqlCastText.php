<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlCastText class file.
 * 
 * This class is a function call to cast a text value to a text.
 * 
 * @author Anastaszor
 * @todo change to StatementVastTextInterface
 */
class MysqlCastText extends StatementFunctionCallText
{
	
	/**
	 * Builds a new MysqlCastText with the given arguments.
	 * 
	 * @param TypeTextInterface $type
	 * @param StatementValueTextInterface $value
	 */
	public function __construct(TypeTextInterface $type, StatementValueTextInterface $value)
	{
		parent::__construct('CAST', $type, [$value]);
	}
	
}

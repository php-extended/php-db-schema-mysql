<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionExp class file.
 * 
 * This class represents the value of e (the base of natural logarithms) raised
 * to the power of X. The inverse of this function is LOG() (using a single
 * argument only) or LN().
 * 
 * @author Anastaszor
 */
class MysqlFunctionExp extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionExp with the given argument.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 */
	public function __construct(StatementValueNumberInterface $arg1)
	{
		parent::__construct('EXP', MysqlTypeNumber::DOUBLE, [$arg1]);
	}
	
}

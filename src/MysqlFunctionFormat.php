<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionFormat class file.
 * 
 * Formats the number X to a format like '#,###,###.##', rounded to D decimal
 * places, and returns the result as a string.
 * 
 * @author Anastaszor
 */
class MysqlFunctionFormat extends StatementFunctionCallString
{
	
	/**
	 * Builds a new MysqlFunctionFormat with the given arguments.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 * @param StatementValueNumberInterface $arg2
	 */
	public function __construct(StatementValueNumberInterface $arg1, StatementValueNumberInterface $arg2)
	{
		parent::__construct('FORMAT', MysqlTypeString::VARCHAR, [$arg1, $arg2]);
	}
	
}

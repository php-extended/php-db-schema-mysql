<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionTan class file.
 * 
 * This class represents the tangent of X, where X is given in radians.
 * 
 * @author Anastaszor
 */
class MysqlFunctionTan extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionTan with the given argument.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 */
	public function __construct(StatementValueNumberInterface $arg1)
	{
		parent::__construct('TAN', MysqlTypeNumber::DOUBLE, [$arg1]);
	}
	
}

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlTypeDatetime class file.
 *
 * This class represents all the valid temporal types for mysql.
 *
 * @author Anastaszor
 */
enum MysqlTypeDatetime : string implements TypeDatetimeInterface
{
	

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeInterface::getName()
	 */
	public function getName() : string
	{
		return $this->value;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeDatetimeInterface::getFormat()
	 */
	public function getFormat() : string
	{
		return match($this)
		{
			self::DATE => 'Y-m-d',
			self::DATETIME => 'Y-m-d H:i:s',
			self::TIME => 'H:i:s',
			self::TIMESTAMP => 'U',
			self::YEAR => 'Y',
		};
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeDatetimeInterface::hasDate()
	 */
	public function hasDate() : bool
	{
		return \strpos($this->value, 'date') !== false;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeDatetimeInterface::hasTime()
	 */
	public function hasTime() : bool
	{
		return \strpos($this->value, 'time') !== false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeDatetimeInterface::castTo()
	 */
	public function castTo(TypeDatetimeInterface $type, StatementValueDatetimeInterface $statement) : StatementValueDatetimeInterface
	{
		if($type->getName() === $this->getName())
		{
			return $statement;
		}
		
		return new MysqlCastDatetime($type, $statement);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeDatetimeInterface::mergeWith()
	 */
	public function mergeWith(TypeDatetimeInterface $type) : TypeDatetimeInterface
	{
		return static::DATETIME;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(TypeVisitorInterface $visitor)
	{
		return $visitor->visitTypeDatetime($this);
	}

	case DATE = 'date';
	case DATETIME = 'datetime';
	case TIME = 'time';
	case TIMESTAMP = 'timestamp';
	case YEAR = 'year';
	
}

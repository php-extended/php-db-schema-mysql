<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionRound class file.
 * 
 * This class represents the argument X to D decimal places. The rounding
 * algorithm depends on the data type of X. D defaults to 0 if not specified.
 * D can be negative to cause D digits left of the decimal point of the value
 * X to become zero. The return value has the same type as the first argument
 * (assuming that it is integer, double, or decimal). This means that for an
 * integer argument, the result is an integer (no decimal places).
 * ROUND() uses the following rules depending on the type of the first argument:
 * For exact-value numbers, ROUND() uses the “round half away from zero” or
 * “round toward nearest” rule: A value with a fractional part of .5 or greater
 * is rounded up to the next integer if positive or down to the next integer if
 * negative. (In other words, it is rounded away from zero.) A value with a
 * fractional part less than .5 is rounded down to the next integer if positive
 * or up to the next integer if negative.
 * For approximate-value numbers, the result depends on the C library. On many
 * systems, this means that ROUND() uses the “round to nearest even” rule: A
 * value with a fractional part exactly half way between two integers is
 * rounded to the nearest even integer.
 * 
 * @author Anastaszor
 */
class MysqlFunctionRound extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionRound with the given arguments.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 * @param StatementValueNumberInterface $arg2
	 */
	public function __construct(StatementValueNumberInterface $arg1, ?StatementValueNumberInterface $arg2 = null)
	{
		parent::__construct('ROUND', MysqlTypeNumber::DECIMAL, (null === $arg2 ? [$arg1] : [$arg1, $arg2]));
	}
	
}

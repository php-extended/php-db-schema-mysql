<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionCot class file.
 * 
 * This class represents the cotangent of X.
 * 
 * @author Anastaszor
 */
class MysqlFunctionCot extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionCot with the given argument.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 */
	public function __construct(StatementValueNumberInterface $arg1)
	{
		parent::__construct('COT', MysqlTypeNumber::DOUBLE, [$arg1]);
	}
	
}

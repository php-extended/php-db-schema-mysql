<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlJsonType class file.
 *
 * This class represents all the valid json types for mysql.
 *
 * @author Anastaszor
 */
enum MysqlTypeJson : string implements TypeJsonInterface
{

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeJsonInterface::getName()
	 */
	public function getName() : string
	{
		return $this->value;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeJsonInterface::getMaxDepths()
	 */
	public function getMaxDepths() : int
	{
		return \PHP_INT_MAX;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeJsonInterface::castTo()
	 */
	public function castTo(TypeJsonInterface $type, StatementValueJsonInterface $statement) : StatementValueJsonInterface
	{
		// as there is only one json type, no need to be cast
		return $statement;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeJsonInterface::mergeWith()
	 */
	public function mergeWith(TypeJsonInterface $type) : TypeJsonInterface
	{
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(TypeVisitorInterface $visitor)
	{
		return $visitor->visitTypeJson($this);
	}

	case JSON = 'json';
	
}

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionTruncate class file.
 * 
 * This class represents the number X, truncated to D decimal places. If D is 0,
 * the result has no decimal point or fractional part. D can be negative to
 * cause D digits left of the decimal point of the value X to become zero. All
 * numbers are rounded toward zero.
 * 
 * @author Anastaszor
 */
class MysqlFunctionTruncate extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionTruncate with the given arguments.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 * @param StatementValueNumberInterface $arg2
	 */
	public function __construct(StatementValueNumberInterface $arg1, StatementValueNumberInterface $arg2)
	{
		parent::__construct('TRUNCATE', MysqlTypeNumber::DECIMAL, [$arg1, $arg2]);
	}
	
}

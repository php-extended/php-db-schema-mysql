<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionLog10 class file.
 * 
 * This class represents the base-10 logarithm of X. LOG10(X) is equivalent
 * to LN(X) / LN(10).
 * 
 * @author Anastaszor
 */
class MysqlFunctionLog10 extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionLog10 with the given arguments.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 */
	public function __construct(StatementValueNumberInterface $arg1)
	{
		parent::__construct('LOG10', MysqlTypeNumber::DOUBLE, [$arg1]);
	}
	
}

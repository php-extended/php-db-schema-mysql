<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionCos class file.
 * 
 * This class represents the cosine of X, where X is given in radians.
 * 
 * @author Anastaszor
 */
class MysqlFunctionCos extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionCos with the given argument.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 */
	public function __construct(StatementValueNumberInterface $arg1)
	{
		parent::__construct('COS', MysqlTypeNumber::DOUBLE, [$arg1]);
	}
	
}

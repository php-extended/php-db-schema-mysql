<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionPi class file.
 * 
 * This class correspond to the value of π (pi). The default number of decimal
 * places displayed is seven, but MySQL uses the full double-precision value
 * internally.
 * 
 * @author Anastaszor
 */
class MysqlFunctionPi extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionPi with no arguments.
	 */
	public function __construct()
	{
		parent::__construct('PI', MysqlTypeNumber::DOUBLE, []);
	}
	
}

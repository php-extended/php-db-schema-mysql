<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

use PhpExtended\Charset\BIG5;
use PhpExtended\Charset\CharacterSetInterface;
use PhpExtended\Charset\CP51932;
use PhpExtended\Charset\DEC_MCS;
use PhpExtended\Charset\EUC_JP;
use PhpExtended\Charset\EUC_KR;
use PhpExtended\Charset\GB18030;
use PhpExtended\Charset\GB2312;
use PhpExtended\Charset\GBK;
use PhpExtended\Charset\HP_ROMAN8;
use PhpExtended\Charset\IBM850;
use PhpExtended\Charset\IBM852;
use PhpExtended\Charset\IBM866;
use PhpExtended\Charset\ISO_8859_1;
use PhpExtended\Charset\ISO_8859_13;
use PhpExtended\Charset\ISO_8859_2;
use PhpExtended\Charset\ISO_8859_7;
use PhpExtended\Charset\ISO_8859_8;
use PhpExtended\Charset\ISO_8859_9;
use PhpExtended\Charset\KOI8_R;
use PhpExtended\Charset\KOI8_U;
use PhpExtended\Charset\MACINTOSH;
use PhpExtended\Charset\SHIFT_JIS;
use PhpExtended\Charset\TIS_620;
use PhpExtended\Charset\US_ASCII;
use PhpExtended\Charset\UTF_16;
use PhpExtended\Charset\UTF_16LE;
use PhpExtended\Charset\UTF_32;
use PhpExtended\Charset\UTF_8;
use PhpExtended\Charset\WINDOWS_1250;
use PhpExtended\Charset\WINDOWS_1251;
use PhpExtended\Charset\WINDOWS_1256;
use PhpExtended\Charset\WINDOWS_1257;
use ReflectionException;

/**
 * MysqlCharacterSet class file.
 *
 * This class represents all valid character sets for mysql.
 *
 * @author Anastaszor
 */
enum MysqlCharacterSet : string
{
	
	/**
	 * Gets a suitable mysql charset for the character given character set.
	 * 
	 * @param CharacterSetInterface $charset
	 * @return MysqlCharacterSet
	 * @throws ReflectionException
	 */
	public static function findByCharset(CharacterSetInterface $charset) : MysqlCharacterSet
	{
		foreach(static::cases() as $mysqlCharset)
		{
			$innerCharset = $mysqlCharset->getNativeCharset();
			if(null !== $innerCharset && $innerCharset->getName() === $charset->getName())
			{
				return $mysqlCharset;
			}
		}
		
		return static::UTF8MB4;
	}
	
	/**
	 * Gets the real character set.
	 * 
	 * @param string $charclass
	 * @return ?CharacterSetInterface
	 */
	protected static function getCharset(string $charclass) : ?CharacterSetInterface
	{
		/** @phpstan-ignore-next-line */
		return CharacterSetReferenceHolder::get()->lookup($charclass);
	}
	
	/**
	 * Gets the characters sets this mysql charset contains.
	 * 
	 * @return ?CharacterSetInterface
	 */
	public function getNativeCharset() : ?CharacterSetInterface
	{
		$class = match($this)
		{
			self::ARMSCII8 => null,
			self::ASCII => US_ASCII::class,
			self::BIG5 => BIG5::class,
			self::BINARY => null,
			self::CP1250 => WINDOWS_1250::class,
			self::CP1251 => WINDOWS_1251::class,
			self::CP1256 => WINDOWS_1256::class,
			self::CP1257 => WINDOWS_1257::class,
			self::CP850 => IBM850::class,
			self::CP852 => IBM852::class,
			self::CP866 => IBM866::class,
			self::CP932 => CP51932::class,
			self::DEC8 => DEC_MCS::class,
			self::EUJPMS => EUC_JP::class,
			self::EUCKR => EUC_KR::class,
			self::GB18030 => GB18030::class,
			self::GB2312 => GB2312::class,
			self::GBK => GBK::class,
			self::GEOSTD8 => null,
			self::GREEK => ISO_8859_7::class,
			self::HEBREW => ISO_8859_8::class,
			self::HP8 => HP_ROMAN8::class,
			self::KEYBCS2 => null,
			self::KOI8R => KOI8_R::class,
			self::KOI8U => KOI8_U::class,
			self::LATIN1 => ISO_8859_1::class,
			self::LATIN2 => ISO_8859_2::class,
			self::LATIN5 => ISO_8859_9::class,
			self::LATIN7 => ISO_8859_13::class,
			self::MACCE => MACINTOSH::class,
			self::MACROMAN => MACINTOSH::class,
			self::SJIS => SHIFT_JIS::class,
			self::SWE7 => null,
			self::TIS620 => TIS_620::class,
			self::UCS2 => null,
			self::UJIS => EUC_JP::class,
			self::UTF16 => UTF_16::class,
			self::UTF16LE => UTF_16LE::class,
			self::UTF32 => UTF_32::class,
			self::UTF8 => UTF_8::class,
			self::UTF8MB4 => UTF_8::class,
		};

		return null !== $class ? self::getCharset($class) : null;
	}

	case ARMSCII8 = 'armscii8';
	case ASCII = 'ascii';
	case BIG5 = 'big5';
	case BINARY = 'binary';
	case CP1250 = 'cp1250';
	case CP1251 = 'cp1251';
	case CP1256 = 'cp1256';
	case CP1257 = 'cp1257';
	case CP850 = 'cp850';
	case CP852 = 'cp852';
	case CP866 = 'cp866';
	case CP932 = 'cp932';
	case DEC8 = 'dec8';
	case EUJPMS = 'eujpms';
	case EUCKR = 'euckr';
	case GB18030 = 'gb18030';
	case GB2312 = 'gb2312';
	case GBK = 'gbk';
	case GEOSTD8 = 'geostd8';
	case GREEK = 'greek';
	case HEBREW = 'hebrew';
	case HP8 = 'hp8';
	case KEYBCS2 = 'keybcs2';
	case KOI8R = 'koi8r';
	case KOI8U = 'koi8u';
	case LATIN1 = 'latin1';
	case LATIN2 = 'latin2';
	case LATIN5 = 'latin5';
	case LATIN7 = 'latin7';
	case MACCE = 'macce';
	case MACROMAN = 'macroman';
	case SJIS = 'sjis';
	case SWE7 = 'swe7';
	case TIS620 = 'tis620';
	case UCS2 = 'ucs2';
	case UJIS = 'ujis';
	case UTF16 = 'utf16';
	case UTF16LE = 'utf16le';
	case UTF32 = 'utf32';
	case UTF8 = 'utf8';
	case UTF8MB4 = 'utf8mb4';
	
}

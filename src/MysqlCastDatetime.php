<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlCastDatetime class file.
 * 
 * This class is a function call to cast a datetime value to a datetime.
 * 
 * @author Anastaszor
 * @todo change to StatementCastDatetimeInterface
 */
class MysqlCastDatetime extends StatementFunctionCallDatetime
{
	
	/**
	 * Builds a new MysqlCastDatetime with the given arguments.
	 * 
	 * @param TypeDatetimeInterface $type
	 * @param StatementValueDatetimeInterface $value
	 */
	public function __construct(TypeDatetimeInterface $type, StatementValueDatetimeInterface $value)
	{
		parent::__construct('CAST', $type, [$value]);
	}
	
}

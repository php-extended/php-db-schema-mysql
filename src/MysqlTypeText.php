<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlTypeText class file.
 *
 * This class represents all valid text types for mysql.
 *
 * @author Anastaszor
 */
enum MysqlTypeText : string implements TypeTextInterface
{

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeInterface::getName()
	 */
	public function getName() : string
	{
		return $this->value;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeTextInterface::getMaxLength()
	 */
	public function getMaxLength() : int
	{
		return match($this)
		{
			self::TINYTEXT => 1,
			self::TINYBLOB => 1,
			self::TEXT => 2,
			self::BLOB => 2,
			self::MEDIUMTEXT => 3,
			self::MEDIUMBLOB => 3,
			self::LONGTEXT => 4,
			self::LONGBLOB => 4,
		};
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeTextInterface::isBinary()
	 */
	public function isBinary() : bool
	{
		return \strpos($this->value, 'blob') !== false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeTextInterface::castTo()
	 */
	public function castTo(TypeTextInterface $type, StatementValueTextInterface $statement) : StatementValueTextInterface
	{
		if($type->getName() === $this->getName())
		{
			return $statement;
		}
		
		return new MysqlCastText($type, $statement);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeTextInterface::mergeWith()
	 */
	public function mergeWith(TypeTextInterface $type) : TypeTextInterface
	{
		if($this->getMaxLength() > $type->getMaxLength())
		{
			return $this;
		}
		
		return $type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(TypeVisitorInterface $visitor)
	{
		return $visitor->visitTypeText($this);
	}
	
	case TINYTEXT = 'tinytext';
	case TEXT = 'text';
	case MEDIUMTEXT = 'mediumtext';
	case LONGTEXT = 'longtext';
	case TINYBLOB = 'tinyblob';
	case BLOB = 'blob';
	case MEDIUMBLOB = 'mediumblob';
	case LONGBLOB = 'longblob';
	
}

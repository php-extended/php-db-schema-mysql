<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionFloor class file.
 * 
 * This class represents the largest integer value not greater than X. For
 * exact-value numeric arguments, the return value has an exact-value numeric
 * type. For string or floating-point arguments, the return value has a
 * floating-point type.
 * 
 * @author Anastaszor
 */
class MysqlFunctionFloor extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionFloor with the given arguments.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 */
	public function __construct(StatementValueNumberInterface $arg1)
	{
		parent::__construct('FLOOR', $arg1->getType(), [$arg1]);
	}
	
}

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlTriggerHandler class file.
 * 
 * This class represents all valid trigger handlers for mysql.
 * 
 * @author Anastaszor
 */
enum MysqlTriggerHandler : string
{

	case BEFORE = 'before';
	case AFTER = 'after';
	
}

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlCastNumber class file.
 * 
 * This class is a function call to cast a number value to a number.
 * 
 * @author Anastaszor
 * @todo change to StatementCastNumberInterface
 */
class MysqlCastNumber extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlCastNumber with the given arguments.
	 * 
	 * @param TypeNumberInterface $type
	 * @param StatementValueNumberInterface $value
	 */
	public function __construct(TypeNumberInterface $type, StatementValueNumberInterface $value)
	{
		parent::__construct('CAST', $type, [$value]);
	}
	
}

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionCrc32 class file.
 * 
 * Computes a cyclic redundancy check value and returns a 32-bit unsigned value.
 * The result is NULL if the argument is NULL. The argument is expected to be a
 * string and (if possible) is treated as one if it is not.
 * 
 * @author Anastaszor
 */
class MysqlFunctionCrc32 extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionCrc32 with the given argument.
	 * 
	 * @param StatementValueStringInterface $expr
	 */
	public function __construct(StatementValueStringInterface $expr)
	{
		parent::__construct('CRC32', MysqlTypeNumber::INT, [$expr]);
	}
	
}

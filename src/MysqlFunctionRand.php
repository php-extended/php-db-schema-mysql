<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionRand class file.
 * 
 * This class represents a random floating-point value v in the range 0 <= v < 1.0.
 * To obtain a random integer R in the range i <= R < j, use the expression
 * FLOOR(i + RAND() * (j − i)). For example, to obtain a random integer in the
 * range the range 7 <= R < 12, use the following statement: 
 * SELECT FLOOR(7 + (RAND() * 5));
 * If an integer argument N is specified, it is used as the seed value:
 * With a constant initializer argument, the seed is initialized once when the
 * statement is prepared, prior to execution.
 * With a nonconstant initializer argument (such as a column name), the seed
 * is initialized with the value for each invocation of RAND().
 * One implication of this behavior is that for equal argument values, RAND(N)
 * returns the same value each time, and thus produces a repeatable sequence of
 * column values. In the following example, the sequence of values produced by
 * RAND(3) is the same both places it occurs.
 * RAND() in a WHERE clause is evaluated for every row (when selecting from one
 * table) or combination of rows (when selecting from a multiple-table join).
 * Thus, for optimizer purposes, RAND() is not a constant value and cannot be
 * used for index optimizations.
 * Use of a column with RAND() values in an ORDER BY or GROUP BY clause may
 * yield unexpected results because for either clause a RAND() expression can
 * be evaluated multiple times for the same row, each time returning a different
 * result. If the goal is to retrieve rows in random order, you can use a
 * statement like this:
 * SELECT * FROM tbl_name ORDER BY RAND();
 * To select a random sample from a set of rows, combine ORDER BY RAND() with
 * LIMIT:
 * SELECT * FROM table1, table2 WHERE a=b AND c<d ORDER BY RAND() LIMIT 1000;
 * RAND() is not meant to be a perfect random generator. It is a fast way to
 * generate random numbers on demand that is portable between platforms for
 * the same MySQL version.
 * This function is unsafe for statement-based replication. A warning is
 * logged if you use this function when binlog_format is set to STATEMENT.
 * 
 * @author Anastaszor
 */
class MysqlFunctionRand extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionRand with the given argument.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 */
	public function __construct(StatementValueNumberInterface $arg1)
	{
		parent::__construct('RAND', MysqlTypeNumber::DOUBLE, [$arg1]);
	}
	
}

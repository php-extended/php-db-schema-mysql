<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlTypeNumber class file.
 *
 * This class represents all valid numeric types for mysql.
 *
 * @author Anastaszor
 */
enum MysqlTypeNumber : string implements TypeNumberInterface
{
	
	/**
	 * Gets the smallest type number with the required length and precision.
	 * 
	 * @param integer $length
	 * @param ?integer $precision
	 * @return MysqlTypeNumber
	 */
	public static function findByLengthAndPrecision(int $length, ?int $precision) : MysqlTypeNumber
	{
		foreach(static::cases() as $value)
		{
			if($value->getMaximumLength() > $length && $value->getMaximumPrecision() > (int) $precision)
			{
				return $value;
			}
		}
		
		if((int) $precision > 0)
		{
			return static::BIGINT;
		}
		
		return static::DOUBLE;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeInterface::getName()
	 */
	public function getName() : string
	{
		return $this->value;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeNumberInterface::getMaximumLength()
	 */
	public function getMaximumLength() : int
	{
		return match($this)
		{
			self::BOOLEAN => 1,
			self::TINYINT => 3,
			self::SMALLINT => 5,
			self::MEDIUMINT => 8,
			self::INT => 10,
			self::BIGINT => 22,
			self::DECIMAL => 65,
			self::FLOAT => 23,
			self::DOUBLE => 53,
			self::REAL => 53,
			self::BIT => 64,
			self::SERIAL => 22,
		};
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeNumberInterface::getMaximumPrecision()
	 */
	public function getMaximumPrecision() : int
	{
		return match($this)
		{
			self::DECIMAL => 30,
			self::FLOAT => 21,
			self::DOUBLE => 51,
			self::REAL => 51,
			default => 0,
		};
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeNumberInterface::castTo()
	 */
	public function castTo(TypeNumberInterface $type, StatementValueNumberInterface $statement) : StatementValueNumberInterface
	{
		if($type->getName() === $type->getName())
		{
			return $statement;
		}
		
		return new MysqlCastNumber($type, $statement);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeNumberInterface::mergeWith()
	 */
	public function mergeWith(TypeNumberInterface $type) : TypeNumberInterface
	{
		return static::findByLengthAndPrecision(
			\max($this->getMaximumLength(), $type->getMaximumLength()),
			\max($this->getMaximumPrecision(), $type->getMaximumPrecision()),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(TypeVisitorInterface $visitor)
	{
		return $visitor->visitTypeNumber($this);
	}

	case BOOLEAN = 'boolean';
	case TINYINT = 'tinyint';
	case SMALLINT = 'smallint';
	case MEDIUMINT = 'mediumint';
	case INT = 'int';
	case BIGINT = 'bigint';
	case DECIMAL = 'decimal';
	case FLOAT = 'float';
	case DOUBLE = 'double';
	case REAL = 'real';
	case BIT = 'bit';
	case SERIAL = 'serial';
	
}

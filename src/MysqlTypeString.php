<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlTypeString class file.
 *
 * This class represents all valid string types for mysql.
 *
 * @author Anastaszor
 */
enum MysqlTypeString : string implements TypeStringInterface
{

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeInterface::getName()
	 */
	public function getName() : string
	{
		return $this->value;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeStringInterface::getMaxLength()
	 */
	public function getMaxLength() : int
	{
		return match($this)
		{
			self::CHAR => 255,
			self::VARCHAR => 65535,
			self::BINARY => 255,
			self::VARBINARY => 65535,
		};
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeStringInterface::isBinary()
	 */
	public function isBinary() : bool
	{
		return \strpos($this->value, 'binary') !== false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeStringInterface::castTo()
	 */
	public function castTo(TypeStringInterface $type, StatementValueStringInterface $statement) : StatementValueStringInterface
	{
		if($type->getName() === $this->getName())
		{
			return $statement;
		}
		
		return new MysqlCastString($type, $statement);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeStringInterface::mergeWith()
	 */
	public function mergeWith(TypeStringInterface $type) : TypeStringInterface
	{
		$isBinary = $this->isBinary() || $type->isBinary();
		$maxLength = \max($this->getMaxLength(), $type->getMaxLength());
		
		return match(true)
		{
			255 >= $maxLength => $isBinary ? MysqlTypeString::BINARY : MysqlTypeString::VARBINARY,
			default => $isBinary ? MysqlTypeString::VARBINARY : MysqlTypeString::VARCHAR,
		};
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TypeInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(TypeVisitorInterface $visitor)
	{
		return $visitor->visitTypeString($this);
	}

	case CHAR = 'char';
	case VARCHAR = 'varchar';
	case BINARY = 'binary';
	case VARBINARY = 'varbinary';
	
}

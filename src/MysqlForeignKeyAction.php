<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlForeignKeyAction class file.
 * 
 * This class represents all the foreign key actions for mysql.
 * 
 * @author Anastaszor
 */
enum MysqlForeignKeyAction : string
{
	
	case CASCADE = 'cascade';
	case NO_ACTION = 'no_action';
	case RESTRICT = 'restrict';
	case SET_NULL = 'set_null';
	
}

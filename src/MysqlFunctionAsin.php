<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionAsin class file.
 * 
 * This class represents the the arc sine of X, that is, the value whose sine
 * is X. Returns NULL if X is not in the range -1 to 1.
 * 
 * @author Anastaszor
 */
class MysqlFunctionAsin extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionAsin with the given argument.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 */
	public function __construct(StatementValueNumberInterface $arg1)
	{
		parent::__construct('ASIN', MysqlTypeNumber::DOUBLE, [$arg1]);
	}
	
}

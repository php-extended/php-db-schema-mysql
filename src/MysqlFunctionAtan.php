<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionAtan class file.
 * 
 * This class represents the arc tangent of X, that is, the value whose tangent
 * is X.
 * 
 * @author Anastaszor
 */
class MysqlFunctionAtan extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionAtan with the given argument.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 */
	public function __construct(StatementValueNumberInterface $arg1)
	{
		parent::__construct('ATAN', MysqlTypeNumber::DOUBLE, [$arg1]);
	}
	
}

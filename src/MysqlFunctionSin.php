<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionSin class file.
 * 
 * This class corresponds to the sine of X, where X is given in radians.
 * 
 * @author Anastaszor
 */
class MysqlFunctionSin extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionSin with the given arguments.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 */
	public function __construct(StatementValueNumberInterface $arg1)
	{
		parent::__construct('SIN', MysqlTypeNumber::DOUBLE, [$arg1]);
	}
	
}

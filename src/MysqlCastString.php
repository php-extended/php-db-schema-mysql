<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlCastString class file.
 * 
 * This class is a function call to cast a string to a string.
 * 
 * @author Anastaszor
 * @todo change to StatementCastStringInterface
 */
class MysqlCastString extends StatementFunctionCallString
{
	
	/**
	 * Builds a new MysqlCastString with the given arguments.
	 * 
	 * @param TypeStringInterface $type
	 * @param StatementValueStringInterface $value
	 */
	public function __construct(TypeStringInterface $type, StatementValueStringInterface $value)
	{
		parent::__construct('CAST', $type, [$value]);
	}
	
}

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionCeiling class file.
 * 
 * This class represents the smallest integer value not less than X. For
 * exact-value numeric arguments, the return value has an exact-value numeric
 * type. For string or floating-point arguments, the return value has a
 * floating-point type.
 * 
 * @author Anastaszor
 */
class MysqlFunctionCeiling extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionCeiling with the given argument.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 */
	public function __construct(StatementValueNumberInterface $arg1)
	{
		parent::__construct('CEILING', $arg1->getType(), [$arg1]);
	}
	
}

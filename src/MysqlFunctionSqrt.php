<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-mysql library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * MysqlFunctionSqrt class file.
 * 
 * This class represents the square root of a nonnegative number X.
 * 
 * @author Anastaszor
 */
class MysqlFunctionSqrt extends StatementFunctionCallNumber
{
	
	/**
	 * Builds a new MysqlFunctionSqrt with the given argument.
	 * 
	 * @param StatementValueNumberInterface $arg1
	 */
	public function __construct(StatementValueNumberInterface $arg1)
	{
		parent::__construct('SQRT', MysqlTypeNumber::DOUBLE, [$arg1]);
	}
	
}

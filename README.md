# php-extended/php-db-schema-mysql
A library to abstract the mysql database schema rules

![coverage](https://gitlab.com/php-extended/php-db-schema-mysql/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-db-schema-mysql/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-db-schema-mysql ^9`


## Basic Usage

This library can generate `CREATE DATABASE` statements.

```php

use PhpExtended\DbSchema\MysqlTableFactory;
use PhpExtended\DbSchema\MysqlDialect;
use PhpExtended\DbSchema\MysqlTypeNumber;
use PhpExtended\DbSchema\MysqlTypeString;

$factory = new MysqlTableFactory();
$dialect = new MysqlDialect();

$factory->setName('<tableName>');

// <tableColumnId> INT(11) NOT NULL AUTO_INCREMENT COMMENT 'The primary key'
$factory->addColumn('<tableColumnId>', MysqlTypeNumber::INT(), 11, null, null, false, true, 'The primary key');
// <nameColumn> VARCHAR(255) NOT NULL COMMENT 'The name of the <xxx>'
$factory->addColumn('<nameColumn>', MysqlTypeString::VARCHAR(), 255, null, null, false, false, 'The name of the <xxx>');
$factory->addsToPrimaryKey('<tableColumnId>');

$sql = $dialect->showCreateTable($factory->getTable());	// CREATE TABLE ...

```


## License

MIT (See [license file](LICENSE)).
